<?php
set_time_limit(0);
include 'logica/conexion.php';
include 'logica/Entrenador.php';
error_reporting (0);

/* @var $_POST type */
$op = $_POST["op"]; //obtenemos el valor de la accion que se esta haciendo
if (isset($op) && $op == "registro") {
    if(comprobar() == false){//si tiene valor...
            $ok = registrar();
    }else{
        header("Location: fallo1.php");
    }
} //.. validamos el ingreso
//sino.. mostrar el formulario
//$ok tendra TRUE si se logeo correctamente
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.png"> 
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        
        <title>Registro de Entrenador</title>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body id="body">
        <section>           
            <div class="container">
                <div id="registro">
                          <h2 class="sombraTexto">Registro de Entrenador</h2>
                          <form method="POST" action="<?php print($_SERVER["PHP_SELF"]);?>"  autocomplete="off">
                              <input type="hidden" name="op" value="registro"/>
                              <br>
                            Nombre Entrenador: <br> <input type="text" name="nomentr"> <br>
                            Género: <br>
                            <select name="genero">
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option>
                            </select><br><br>
                            <input type="submit" value="Registrarse">                          
                          </form><br>
                          <form action="index.php">
                            <input type="submit" value="Cancelar">
                          </form><br>
                </div>
              </div>           
        </section>
    </body>
</html>
<?php
function comprobar(){
    $usuario=$_POST["nomentr"];
    $conn = conectar();
//creamos un comando SQL, notar que si pongo comillas dobles, el valor de las variables
//   son interpretadas como parte de la cadena
$query="SELECT ID FROM entrenador WHERE Nombre = '$usuario'";
$res=mysqli_query($conn, $query) or die (mysqli_error($conn)); //ejecuto el comando

$resultado = mysqli_data_seek($res, 0);

if ($resultado){
    return true;
}else{
    return false;
    }
}

function regipoke(){
    $usuario=$_POST["nomentr"]; //obtengo el parametro usuario del formulario...
    $genero=$_POST["genero"];
    $var = new Entrenador($usuario, $genero);
    $var->asignar_Equipo();
    $conn = conectar();
    $ident += $_SESSION["IDen"];
    $idpokemon = 0;
    
    for($index = 0; $index < 6; $index ++){
        
        $nombrepoke = $var->getEquipo_Pokemon()[$index]->getNombre();
        $idpoke = $var->getEquipo_Pokemon()[$index]->getId();
       
        $mov1 = $var->getEquipo_Pokemon()[$index]->getMovimientos()[0];
        $mov2 = $var->getEquipo_Pokemon()[$index]->getMovimientos()[1];
        $mov3 = $var->getEquipo_Pokemon()[$index]->getMovimientos()[2];
        $mov4 = $var->getEquipo_Pokemon()[$index]->getMovimientos()[3];  
        
        $queryh = "INSERT INTO pokemon VALUES (0,$idpoke,'$nombrepoke','$mov1','$mov2','$mov3','$mov4')" ;
        $resh= mysqli_query($conn, $queryh) or die (mysqli_error($conn)); //ejecuto el comando
        
        $queryid ="SELECT * FROM pokemon WHERE Nombre = '$nombrepoke' ORDER BY ID DESC";
        $resid =mysqli_query($conn, $queryid) or die (mysqli_error($conn));
        
        if ($reg= mysqli_fetch_object($resid)){ //obtengo todo el registro como un objeto
        $_SESSION["IDpoke"]= $reg->ID;
        $idpokemon = 0;
        }
        
        $idpokemon += $_SESSION["IDpoke"];
        
        $queryf = "INSERT INTO amistad VALUES (0,$ident,$idpokemon)";
        $resf= mysqli_query($conn, $queryf) or die (mysqli_error($conn)); //ejecuto el comando
    
}
    
}

function registrar(){
$usuario=$_POST["nomentr"]; //obtengo el parametro usuario del formulario...
$genero=$_POST["genero"];
$conn = conectar();

//creamos un comando SQL, notar que si pongo comillas dobles, el valor de las variables
//   son interpretadas como parte de la cadena
$query="INSERT INTO entrenador VALUES (0,'$usuario','$genero')";
$res=mysqli_query($conn, $query) or die (mysqli_error($conn)); //ejecuto el comando

$querysf ="SELECT * FROM entrenador WHERE Nombre = '$usuario'";
$ressf =mysqli_query($conn, $querysf) or die (mysqli_error($conn));
        
if ($reg= mysqli_fetch_object($ressf)){ //obtengo todo el registro como un objeto
    $_SESSION["IDen"]= $reg->ID;
    }

if ($res ){ //.. si se ejecuto correctamente, el valor de $res no es fals        
        regipoke();
        header("Location: comprobacion.php");
    }
}

?>
