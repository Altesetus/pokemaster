<?php
session_start();
set_time_limit(0);
include 'logica/conexion.php';
include 'logica/EntrenadorArtificial.php';
error_reporting (0);

$op = $_POST["op"]; //obtenemos el valor de la accion que se esta haciendo
if (isset($op) && $op == "registro") {
    if(comprobar() == false){//si tiene valor y es 'login'...
    $ok = registrar();
    }else{
        header("Location: fallo2.php");
    }
}

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
}else { 
    header("Location: index.php");
}
$now = time();

if($now > $_SESSION['expire']) {
session_destroy();
    }

function listado(){
    $consulta = new ConsultarApi();
    $json = $consulta->listaPokes();
    $pokes = json_decode($json, true);
    $consulta->llenarArrayPokes($json);
    $auxdex = 0;
    for($index = 0; $index < 801; $index ++){
        $auxdex = 1 + $index;
        $nombrepoke = "".$consulta->getPokemons()[$index];
    echo "<option value = '$auxdex'>$nombrepoke</option>";
    }
}

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.png"> 
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        
        <title>Agregar IA</title>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <section>           
            <div class="container">
                <div id="equipoIA">
                          <h2 class="sombraTexto">Crear Entrenador Artificial</h2>
                          <form method="POST" action="<?php print($_SERVER["PHP_SELF"]);?>"  autocomplete="off">
                              <input type="hidden" name="op" value="registro"/>
                          Nombre Entrenador: <br> <input type="text" name="nomentr"> <br>
                          Dificultad: <br>
                            <select name="dific">
                                <option value=1>Fácil</option>
                                <option value=2>Normal</option>
                                <option value=3>Pokémaster</option>
                            </select><br><br>
                            
                            <p>Por favor, elija los 6 Pokémon del entrenador Artificial:
                          <table id="tabla1">
                              <tr id="th1">
                                  <td id="td2">
                                      <select name='poke1'>
                                      <?php 
                                      listado();
                                      ?>
                                      </select>
                                  </td><br>
                                  <td id="td2">
                                      <select name='poke2'>
                                      <?php 
                                      listado();
                                      ?>
                                      </select>
                                  </td><br>
                                  <td id="td2">
                                      <select name='poke3'>
                                      <?php 
                                      listado();
                                      ?>
                                      </select>
                                  </td><br>
                              </tr>
                              <tr id="th1">
                                  <td id="td2">
                                      <select name='poke4'>
                                      <?php 
                                      listado();
                                      ?>
                                      </select>
                                  </td><br>
                              <td id="td2">
                                      <select name='poke5'>
                                      <?php 
                                      listado();
                                      ?>
                                      </select>
                                  </td><br>
                                  <td id="td2">
                                      <select name='poke6'>
                                      <?php 
                                      listado();
                                      ?>
                                      </select>
                                  </td><br>
                              </tr>
                          </table>
                            <input type="submit" value="Guardar">
                          </form><br>
                          <form action="inicio.php">
                            <input type="submit" value="Cancelar">
                          </form><br>
                </div>
              </div>           
        </section>
    </body>
</html>
<?php

function comprobar(){
    $usuario= $_POST["nomentr"];
    $conn = conectar();
//creamos un comando SQL, notar que si pongo comillas dobles, el valor de las variables
//   son interpretadas como parte de la cadena
$query="SELECT ID FROM entrenadorart WHERE Nombre = '$usuario'";
$res=mysqli_query($conn, $query) or die (mysqli_error($conn)); //ejecuto el comando

$resultado = mysqli_data_seek($res, 0);

if ($resultado){
    return true;
}else{
    return false;
    }
}


function regipokeIA(){
    $conn = conectar();
    $identart += $_SESSION["IDenart"];
    $dif= $_POST["dific"];
    $usuario= $_POST["nomentr"];
    
    $idpoke1 += $_POST["poke1"];
    $idpoke2 += $_POST["poke2"];
    $idpoke3 += $_POST["poke3"];
    $idpoke4 += $_POST["poke4"];
    $idpoke5 += $_POST["poke5"];
    $idpoke6 += $_POST["poke6"];
    $idpokemonart = 0;
    
    $var = new EntrenadorArtificial($usuario, $dif);
    
    $var->seleccionar_Equipo($idpoke1,$idpoke2,$idpoke3,$idpoke4,$idpoke5,$idpoke6);
    
    for($index = 0; $index < 6; $index ++){
        
        $nombrepoke = $var->getEquipo_Pokemon()[$index]->getNombre();
        $idpoke = $var->getEquipo_Pokemon()[$index]->getId();
       
        $mov1 = $var->getEquipo_Pokemon()[$index]->getMovimientos()[0];
        $mov2 = $var->getEquipo_Pokemon()[$index]->getMovimientos()[1];
        $mov3 = $var->getEquipo_Pokemon()[$index]->getMovimientos()[2];
        $mov4 = $var->getEquipo_Pokemon()[$index]->getMovimientos()[3];  
        
        $queryhart = "INSERT INTO pokemon VALUES (0,$idpoke,'$nombrepoke','$mov1','$mov2','$mov3','$mov4')" ;
        $reshart = mysqli_query($conn, $queryhart) or die (mysqli_error($conn)); //ejecuto el comando
        
        $queryidart = "SELECT * FROM pokemon WHERE Nombre = '$nombrepoke' ORDER BY ID DESC";
        $residart = mysqli_query($conn, $queryidart) or die (mysqli_error($conn));
        
        if ($regart= mysqli_fetch_object($residart)){ //obtengo todo el registro como un objeto
        $_SESSION["IDpokem"]= $regart->ID;
        $idpokemonart = 0;
        }
        
        $idpokemonart += $_SESSION["IDpokem"];
        
        $queryfart = "INSERT INTO relacion VALUES (0,$identart,$idpokemonart)";
        $resfart = mysqli_query($conn, $queryfart) or die (mysqli_error($conn)); //ejecuto el comando
    
    }
}

function registrar(){
$usuario= $_POST["nomentr"]; //obtengo el parametro usuario del formulario...
$dif= $_POST["dific"];
$conn = conectar();
//creamos un comando SQL, notar que si pongo comillas dobles, el valor de las variables
//   son interpretadas como parte de la cadena
$query="INSERT INTO entrenadorart VALUES (0,'$usuario','$dif')";
$res=mysqli_query($conn, $query) or die (mysqli_error($conn)); //ejecuto el comando

$querysf ="SELECT * FROM entrenadorart WHERE Nombre = '$usuario'";
$ressf =mysqli_query($conn, $querysf) or die (mysqli_error($conn));
        
if ($reg= mysqli_fetch_object($ressf)){ //obtengo todo el registro como un objeto
    $_SESSION["IDenart"]= $reg->ID;
    }

if ($res ){ //.. si se ejecuto correctamente, el valor de $res no es falso
    regipokeIA();
    header("Location: comprobacion1.php");
    }
}

?>