-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-12-2017 a las 20:33:06
-- Versión del servidor: 10.1.29-MariaDB
-- Versión de PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pokemaster`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `amistad`
--

CREATE TABLE `amistad` (
  `IDamistad` int(10) NOT NULL,
  `IDentrenador` int(10) NOT NULL,
  `IDpokemon` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `amistad`
--

INSERT INTO `amistad` (`IDamistad`, `IDentrenador`, `IDpokemon`) VALUES
(24, 39, 91),
(25, 39, 92),
(26, 39, 93),
(27, 39, 94),
(28, 39, 95),
(29, 39, 96),
(30, 40, 115),
(31, 40, 116),
(32, 40, 117),
(33, 40, 118),
(34, 40, 119),
(35, 40, 120),
(36, 41, 121),
(37, 41, 122),
(38, 41, 123),
(39, 41, 124),
(40, 41, 125),
(41, 41, 126),
(42, 42, 133),
(43, 42, 134),
(44, 42, 135),
(45, 42, 136),
(46, 42, 137),
(47, 42, 138);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `batalla`
--

CREATE TABLE `batalla` (
  `IDbatalla` int(10) NOT NULL,
  `IDentrenador` int(10) NOT NULL,
  `IDentrenadorart` int(10) NOT NULL,
  `nombrevencedor` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `batalla`
--

INSERT INTO `batalla` (`IDbatalla`, `IDentrenador`, `IDentrenadorart`, `nombrevencedor`) VALUES
(1, 39, 32, 'Red'),
(2, 40, 31, 'Aliza'),
(3, 40, 30, 'Green'),
(4, 40, 32, 'Red'),
(5, 39, 31, 'Luka'),
(6, 39, 30, 'Green'),
(7, 39, 32, 'Luka'),
(8, 41, 32, 'Ash'),
(9, 41, 32, 'Ash'),
(10, 41, 31, 'Ash'),
(11, 41, 31, 'Ash'),
(12, 41, 33, 'Giovany'),
(14, 42, 33, 'Giovany');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrenador`
--

CREATE TABLE `entrenador` (
  `ID` int(10) NOT NULL,
  `Nombre` varchar(25) NOT NULL,
  `Genero` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entrenador`
--

INSERT INTO `entrenador` (`ID`, `Nombre`, `Genero`) VALUES
(39, 'Luka', 'M'),
(40, 'Aliza', 'F'),
(41, 'Ash', 'M'),
(42, 'Joshua', 'M');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrenadorart`
--

CREATE TABLE `entrenadorart` (
  `ID` int(10) NOT NULL,
  `Nombre` varchar(25) NOT NULL,
  `Nivel` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entrenadorart`
--

INSERT INTO `entrenadorart` (`ID`, `Nombre`, `Nivel`) VALUES
(30, 'Green', 1),
(31, 'Blue', 2),
(32, 'Red', 3),
(33, 'Giovany', 3),
(34, 'Jhonny', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pokemon`
--

CREATE TABLE `pokemon` (
  `ID` int(10) NOT NULL,
  `IDpoke` int(10) NOT NULL,
  `Nombre` varchar(25) NOT NULL,
  `movimiento1` varchar(20) NOT NULL,
  `movimiento2` varchar(20) NOT NULL,
  `movimiento3` varchar(20) NOT NULL,
  `movimiento4` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pokemon`
--

INSERT INTO `pokemon` (`ID`, `IDpoke`, `Nombre`, `movimiento1`, `movimiento2`, `movimiento3`, `movimiento4`) VALUES
(91, 25, 'pikachu', 'brick-break', 'feather-dance', 'steel-wing', 'aerial-ace'),
(92, 205, 'forretress', 'snore', 'hyper-beam', 'protect', 'toxic'),
(93, 75, 'graveler', 'sandstorm', 'flash', 'rock-slide', 'protect'),
(94, 593, 'jellicent', 'bubble-beam', 'toxic', 'thunder', 'dig'),
(95, 98, 'krabby', 'hidden-power', 'protect', 'water-gun', 'sleep-talk'),
(96, 307, 'meditite', 'splash', 'water-gun', 'blizzard', 'waterfall'),
(97, 3, 'venusaur', 'scary-face', 'focus-energy', 'fire-blast', 'fury-swipes'),
(98, 24, 'arbok', 'vine-whip', 'light-screen', 'reflect', 'string-shot'),
(99, 49, 'venomoth', 'sleep-talk', 'snore', 'wish', 'return'),
(100, 85, 'dodrio', 'wrap', 'snore', 'sludge-bomb', 'rock-throw'),
(101, 95, 'onix', 'thunder-wave', 'headbutt', 'thunder-punch', 'cut'),
(102, 65, 'alakazam', 'mean-look', 'reflect', 'solar-beam', 'skull-bash'),
(103, 9, 'blastoise', 'mimic', 'psychic', 'ice-punch', 'rage'),
(104, 12, 'butterfree', 'dream-eater', 'attract', 'hidden-power', 'snore'),
(105, 15, 'beedrill', 'mud-slap', 'toxic', 'roar', 'solar-beam'),
(106, 18, 'pidgeot', 'psychic', 'flash', 'present', 'light-screen'),
(107, 105, 'marowak', 'charm', 'pursuit', 'double-team', 'return'),
(108, 135, 'jolteon', 'bite', 'focus-energy', 'ice-beam', 'defense-curl'),
(109, 25, 'pikachu', 'hidden-power', 'solar-beam', 'light-screen', 'absorb'),
(110, 6, 'charizard', 'protect', 'growl', 'water-gun', 'blizzard'),
(111, 149, 'dragonite', 'crunch', 'reflect', 'ice-beam', 'bubble'),
(112, 107, 'hitmonchan', 'secret-power', 'bubble', 'surf', 'surf'),
(113, 112, 'rhydon', 'horn-attack', 'counter', 'horn-attack', 'fissure'),
(114, 143, 'snorlax', 'mimic', 'water-gun', 'fire-punch', 'fissure'),
(115, 25, 'pikachu', 'seismic-toss', 'skull-bash', 'double-team', 'seismic-toss'),
(116, 458, 'mantyke', 'double-team', 'curse', 'swift', 'protect'),
(117, 620, 'mienshao', 'protect', 'rain-dance', 'sleep-talk', 'rain-dance'),
(118, 681, 'aegislash-shield', 'false-swipe', 'snore', 'substitute', 'fury-attack'),
(119, 194, 'wooper', 'cut', 'rest', 'body-slam', 'toxic'),
(120, 772, 'type-null', 'spark', 'spark', 'explosion', 'double-team'),
(121, 25, 'pikachu', 'toxic', 'screech', 'night-shade', 'thief'),
(122, 195, 'quagsire', 'protect', 'return', 'destiny-bond', 'explosion'),
(123, 528, 'swoobat', 'strength', 'screech', 'strength', 'psychic'),
(124, 290, 'nincada', 'surf', 'bide', 'double-edge', 'mimic'),
(125, 569, 'garbodor', 'snore', 'rock-slide', 'protect', 'rest'),
(126, 197, 'umbreon', 'submission', 'harden', 'mega-kick', 'body-slam'),
(127, 6, 'charizard', 'fire-blast', 'protect', 'thunder-punch', 'blizzard'),
(128, 12, 'butterfree', 'endeavor', 'wish', 'hidden-power', 'attract'),
(129, 15, 'beedrill', 'frustration', 'dream-eater', 'rest', 'frustration'),
(130, 94, 'gengar', 'toxic', 'body-slam', 'smokescreen', 'protect'),
(131, 229, 'houndoom', 'bite', 'fire-blast', 'mud-slap', 'sludge-bomb'),
(132, 386, 'deoxys-normal', 'mega-punch', 'submission', 'blizzard', 'thunder-wave'),
(133, 25, 'pikachu', 'swift', 'blizzard', 'agility', 'horn-drill'),
(134, 231, 'phanpy', 'leer', 'substitute', 'rest', 'thunder-punch'),
(135, 581, 'swanna', 'sand-attack', 'solar-beam', 'cut', 'swords-dance'),
(136, 425, 'drifloon', 'protect', 'self-destruct', 'ice-punch', 'nightmare'),
(137, 90, 'shellder', 'hyper-beam', 'screech', 'hyper-beam', 'take-down'),
(138, 261, 'poochyena', 'hyper-beam', 'rock-slide', 'thrash', 'strength'),
(139, 4, 'charmander', 'swift', 'hyper-beam', 'curse', 'double-edge'),
(140, 3, 'venusaur', 'curse', 'curse', 'double-edge', 'mimic'),
(141, 9, 'blastoise', 'payback', 'substitute', 'hidden-power', 'attract'),
(142, 20, 'raticate', 'sleep-talk', 'mirror-coat', 'blizzard', 'facade'),
(143, 18, 'pidgeot', 'poison-powder', 'snore', 'poison-powder', 'string-shot'),
(144, 12, 'butterfree', 'surf', 'mega-kick', 'flamethrower', 'rage');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `relacion`
--

CREATE TABLE `relacion` (
  `IDrelacion` int(10) NOT NULL,
  `IDentrenadorart` int(10) NOT NULL,
  `IDpokemon` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `relacion`
--

INSERT INTO `relacion` (`IDrelacion`, `IDentrenadorart`, `IDpokemon`) VALUES
(66, 30, 97),
(67, 30, 98),
(68, 30, 99),
(69, 30, 100),
(70, 30, 101),
(71, 30, 102),
(72, 31, 103),
(73, 31, 104),
(74, 31, 105),
(75, 31, 106),
(76, 31, 107),
(77, 31, 108),
(78, 32, 109),
(79, 32, 110),
(80, 32, 111),
(81, 32, 112),
(82, 32, 113),
(83, 32, 114),
(84, 33, 127),
(85, 33, 128),
(86, 33, 129),
(87, 33, 130),
(88, 33, 131),
(89, 33, 132),
(90, 34, 139),
(91, 34, 140),
(92, 34, 141),
(93, 34, 142),
(94, 34, 143),
(95, 34, 144);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `amistad`
--
ALTER TABLE `amistad`
  ADD PRIMARY KEY (`IDamistad`),
  ADD KEY `IDentrenador` (`IDentrenador`),
  ADD KEY `IDpokemon` (`IDpokemon`);

--
-- Indices de la tabla `batalla`
--
ALTER TABLE `batalla`
  ADD PRIMARY KEY (`IDbatalla`),
  ADD KEY `IDentrenador` (`IDentrenador`),
  ADD KEY `IDentrenadorart` (`IDentrenadorart`);

--
-- Indices de la tabla `entrenador`
--
ALTER TABLE `entrenador`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `entrenadorart`
--
ALTER TABLE `entrenadorart`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `pokemon`
--
ALTER TABLE `pokemon`
  ADD PRIMARY KEY (`ID`) USING BTREE;

--
-- Indices de la tabla `relacion`
--
ALTER TABLE `relacion`
  ADD PRIMARY KEY (`IDrelacion`),
  ADD KEY `IDentrenadorart` (`IDentrenadorart`),
  ADD KEY `IDpokemon` (`IDpokemon`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `amistad`
--
ALTER TABLE `amistad`
  MODIFY `IDamistad` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `batalla`
--
ALTER TABLE `batalla`
  MODIFY `IDbatalla` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `entrenador`
--
ALTER TABLE `entrenador`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `entrenadorart`
--
ALTER TABLE `entrenadorart`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `pokemon`
--
ALTER TABLE `pokemon`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT de la tabla `relacion`
--
ALTER TABLE `relacion`
  MODIFY `IDrelacion` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `amistad`
--
ALTER TABLE `amistad`
  ADD CONSTRAINT `amistad_ibfk_1` FOREIGN KEY (`IDentrenador`) REFERENCES `entrenador` (`ID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `amistad_ibfk_2` FOREIGN KEY (`IDpokemon`) REFERENCES `pokemon` (`ID`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `batalla`
--
ALTER TABLE `batalla`
  ADD CONSTRAINT `batalla_ibfk_1` FOREIGN KEY (`IDentrenador`) REFERENCES `entrenador` (`ID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `batalla_ibfk_2` FOREIGN KEY (`IDentrenadorart`) REFERENCES `entrenadorart` (`ID`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `relacion`
--
ALTER TABLE `relacion`
  ADD CONSTRAINT `relacion_ibfk_1` FOREIGN KEY (`IDentrenadorart`) REFERENCES `entrenadorart` (`ID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `relacion_ibfk_2` FOREIGN KEY (`IDpokemon`) REFERENCES `pokemon` (`ID`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
