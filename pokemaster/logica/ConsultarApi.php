<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConsultarApi
 *
 * @author Andres
 */
class ConsultarApi {
    
    private $pokemons = array();
    
    //Contructor
    public function __construct() { 
        $this->baseUrl = 'http://pokeapi.co/api/v2/';
    }
    
    //Metodos
    function getPokemons() {
        return $this->pokemons;
    }

    function setPokemons($pokemons) {
        $this->pokemons = $pokemons;
    }

        
    public function consultarPokemon(){
        $id = random_int(1, 802);
        $url = $this->baseUrl.'pokemon/'.$id;
        return $this->sendRequest($url);
        
    }
    
    public function mostrarId($json){
        $obj = json_decode($json, true);
        return $obj['id'];//'forms'][0]['name'];
        //return $obj['name'];
    }
    
    function mostrarNombrePok($json){
        $obj = json_decode($json, true);
        return $obj['name'];
    }
    
    public function consultarPoke($id){        
        $url = $this->baseUrl.'pokemon/'.$id;
        return $this->sendRequest($url);
    }  
    
    Public function consultarMovimientos($id){
        //$id = random_int(1, 50);
        $url = $this->baseUrl.'pokemon/'.$id;
        return $this->sendRequest($url);
    }
    
    public function mostrarMov($json){
        $mov = random_int(1, 30);
        $obj = json_decode($json, true);
        return $obj['moves'][$mov]['move']['name'];
    }
    
    public function consultarSprite($id){        
        $url = $this->baseUrl.'pokemon-form/'.$id;
        return $this->sendRequest($url);
    }
    /*
    public function obtenerNombrePoke($id){
        $url = $this->baseUrl.'pokemon/'.$id;
        return $this->sendRequest($url);
    }*/
    
    public function listaPokes(){
        $url = $this->baseUrl.'pokemon/?limit=801&offset=801"';
        return $this->sendRequest($url);
    }
    
    public function llenarArrayPokes($json){
        $obj = json_decode($json, true);
        for ($index = 0; $index < 801; $index++) {
            $this->pokemons[]= $obj['results'][$index]['name'];
        }
    }
    
    public function sendRequest($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($http_code != 200) {
            return json_encode('An error has occured.');
        }
        return $data;
    }


}

//Prueba mostrarId
/*
$consulta = new ConsultarApi();
$json = $consulta->consultarPokemon();
echo $consulta->mostrarId($json);
*/

//Prueba mostrarNombrePok
/*
$consulta = new ConsultarApi();
$json = $consulta->consultarPokemon();
echo $consulta->mostrarNombrePok($json);
 */


//Prueba mostrarMov

//$consulta = new ConsultarApi();
//$json = $consulta->consultarMovimientos(1);
//$consulta->mostrarMov($json);

//Prueba ConsultarMovs
/*
$consulta = new ConsultarApi();
$json = $consulta->consultarMovimientos(1);
$cosa = json_decode($json, true);
$mov = random_int(1, 50);
echo $cosa['moves'][$mov]['move']['name'];
*/

//Prueba Sprites

//$consulta = new ConsultarApi();
//$json = $consulta->consultarSprite(1);
//$cosa = json_decode($json, true);
//echo $cosa['sprites']['front_default']; 
 

//Prueba Obtener ID
/*
$consulta = new ConsultarApi();
$json = $consulta->obtenerIdPoke("pikachu");
$cosa = json_decode($json, true);
echo $cosa['id'];
 */

//Prueba listar pokemon
/*
$consulta = new ConsultarApi();
$json = $consulta->listaPokes();
$cosa = json_decode($json, true);
$consulta->llenarArrayPokes($json);

echo "".$consulta->getPokemons()[0];
echo "".$consulta->getPokemons()[99];
*/