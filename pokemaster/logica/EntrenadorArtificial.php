<?php
set_time_limit(0);
/**
 * Description of EntrenadorArtificial
 *
 * @author Andres
 */
//include 'ConsultarApi.php';
//$obj = new ConsultarApi();
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});

class EntrenadorArtificial {
    //Atributos
    private $nombre;
    private $imagen;
    private $nivel;
    private $equipo_Pokemon = array();
    
    
    //Constructor
    function __construct($nombre, $nivel) {
        $this->nombre = $nombre;
        $this->nivel = $nivel;        
    }
    
    //Metodos
    function getNombre() {
        return $this->nombre;
    }

    function getImagen() {
        return $this->imagen;
    }

    function getNivel() {
        return $this->nivel;
    }

    function getEquipo_Pokemon() {
        return $this->equipo_Pokemon;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setImagen($imagen) {
        $this->imagen = $imagen;
    }

    function setNivel($nivel) {
        $this->nivel = $nivel;
    }

    function setEquipo_Pokemon($equipo_Pokemon) {
        $this->equipo_Pokemon = $equipo_Pokemon;
    }

    function seleccionar_Equipo($n1, $n2, $n3, $n4, $n5, $n6){
        //Pendiente
        $consulta1 = new ConsultarApi();
        
        $json1 = $consulta1->consultarPoke($n1);
        //$id = $consulta->mostrarId($json2);
        $pokemon1 = new Pokemon($n1);
        $name1 = $consulta1->mostrarNombrePok($json1);
        $pokemon1->setNombre($name1);
        $pokemon1->aprender_movs($n1);         
        $this->equipo_Pokemon[]= $pokemon1;
        
        $json2 = $consulta1->consultarPoke($n2);
        $pokemon2 = new Pokemon($n2);
        $name2 = $consulta1->mostrarNombrePok($json2);
        $pokemon2->setNombre($name2);
        $pokemon2->aprender_movs($n2);            
        $this->equipo_Pokemon[]= $pokemon2;
        
        $json3 = $consulta1->consultarPoke($n3);
        $pokemon3 = new Pokemon($n3);
        $name3 = $consulta1->mostrarNombrePok($json3);
        $pokemon3->setNombre($name3);
        $pokemon3->aprender_movs($n3);            
        $this->equipo_Pokemon[]= $pokemon3;
        
        $json4 = $consulta1->consultarPoke($n4);
        $pokemon4 = new Pokemon($n4);
        $name4 = $consulta1->mostrarNombrePok($json4);
        $pokemon4->setNombre($name4);
        $pokemon4->aprender_movs($n4);            
        $this->equipo_Pokemon[]= $pokemon4;
        
        $json5 = $consulta1->consultarPoke($n5);
        $pokemon5 = new Pokemon($n5);
        $name5 = $consulta1->mostrarNombrePok($json5);
        $pokemon5->setNombre($name5);
        $pokemon5->aprender_movs($n5);            
        $this->equipo_Pokemon[]= $pokemon5;
        
        $json6 = $consulta1->consultarPoke($n6);
        $pokemon6 = new Pokemon($n6);
        $name6 = $consulta1->mostrarNombrePok($json6);
        $pokemon6->setNombre($name6);
        $pokemon6->aprender_movs($n6);            
        $this->equipo_Pokemon[]= $pokemon6;   
              
    }
}

//La prueba funciona dentro del tiempo de ejecucion con los 2 primeros pokemon
//$var = new EntrenadorArtificial("Hulk", 2);
//$var->seleccionar_Equipo(1, 2,3,4,5,6);

//echo " ".$var->getEquipo_Pokemon()[0]->getNombre();
//echo " ".$var->getEquipo_Pokemon()[1]->getNombre();
//echo " ".$var->getEquipo_Pokemon()[2]->getNombre();
//echo " ".$var->getEquipo_Pokemon()[3]->getNombre();
//echo " ".$var->getEquipo_Pokemon()[4]->getNombre();
//echo " ".$var->getEquipo_Pokemon()[5]->getNombre();
 
 