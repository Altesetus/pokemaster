<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Duelo
 *
 * @author Andres
 */

/*include 'Entrenador.php';
include 'EntrenadorArtificial.php';
include 'Pokemon.php';
*/
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});

//$jugador = new Entrenador("Andres","M");
//$maquina = new EntrenadorArtificial("maquina", 1);

class Duelo {
    private $nivel = 1;
    private $turno = 1;
    private $nPokJ1= 0;
    private $nPokJ2= 0;
    private $vencedor;
    
    function __construct($nivel) {
        $this->nivel = $nivel;
    }
    
    public function duelo($nickname, $genero, $nombre, $nivel){
        $entrenador = new Entrenador($nickname, $genero);
        $maquina = new EntrenadorArtificial($nombre, $nivel);
        
        $critico = 10;
        if ($nivel == 1) {
            $critico = 20;
        }
        if ($nivel == 2) {
            $critico = 40;
        }
        if ($nivel == 1) {
            $critico = 70;
        }
        
    }
    
    public function elegirAtaque($jugador, $num){ //Recibe un jugador y la posicion del pokemon que atacará
        $rand = random_int(0, 3);
        $mov = $jugador->getEquipoPokemon()[$num]->getMovimientos()[$rand];
        return $mov;
    }

    
    
}

//Prueba elegirAtaque
$obj1 = new Entrenador("jhonnyh748", "m");
$obj1->asignar_Equipo();
echo elegirAtaque($obj1, 1);

