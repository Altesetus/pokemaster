<?php
/**
 * Description of Pokemon
 *
 * @author Andres
 */
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});
//include 'ConsultarApi.php';
//$objct = new ConsultarApi();
class Pokemon {
    //Atributos
    private $id;
    private $nombre;
    private $imagen;
    private $hp =100;
    private $movimientos = array();
    
    //Constructor
    function __construct($id) {
        $this->id = $id;
        
    }
    
    //Metodos
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getImagen() {
        return $this->imagen;
    }

    function getHp() {
        return $this->hp;
    }

    function getMovimientos() {
        return $this->movimientos;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setImagen($imagen) {
        $this->imagen = $imagen;
    }

    function setHp($hp) {
        $this->hp = $hp;
    }

    function setMovimientos($movimientos) {
        $this->movimientos = $movimientos;
    }   

    function aprender_movs($id){
        //Pendiente
        $consulta = new ConsultarApi();
        $json = $consulta->consultarPokemon($id);
        for ($index = 0; $index < 5; $index++) {            
            $this->movimientos[]= $consulta->mostrarMov($json);
        }
    
    }

}

//$var = new Pokemon(80);
//$var->aprender_movs();

//echo " ".$var->getMovimientos()[1];
//echo " ".$var->getMovimientos()[2];
//echo " ".$var->getMovimientos()[3];
 
 