<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Entrenador
 *
 * @author Andres
 */

//include 'ConsultarApi.php';
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});
//$obj = new ConsultarApi();

class Entrenador {
    //put your code here
    
    //Atributos
    private $id;
    private $nickname;
    private $genero;
    private $batallas_Ganadas;
    public $equipo_Pokemon = array();
    
    //Constructor
    
    function __construct($nickname, $genero) {
        $this->nickname = $nickname;
        $this->genero = $genero;
    }
    
    //Metodos
    function getId() {
        return $this->id;
    }

    function getNickname() {
        return $this->nickname;
    }

    function getGenero() {
        return $this->genero;
    }

    function getBatallas_Ganadas() {
        return $this->batallas_Ganadas;
    }

    function getEquipo_Pokemon() {
        return $this->equipo_Pokemon;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNickname($nickname) {
        $this->nickname = $nickname;
    }

    function setGenero($genero) {
        $this->genero = $genero;
    }

    function setBatallas_Ganadas($batallas_Ganadas) {
        $this->batallas_Ganadas = $batallas_Ganadas;
    }

    function setEquipo_Pokemon($equipo_Pokemon) {
        $this->equipo_Pokemon = $equipo_Pokemon;
    }

    function asignar_Equipo(){ //Asigna un equipo a un jugador y cada pokemon con sus respectivos movimientos.
        $consulta = new ConsultarApi();
        $idi = 25;
        $json = $consulta->consultarPoke($idi);
        
        $inicial = new Pokemon($idi);
        $inicial->setNombre($consulta->mostrarNombrePok($json));
        $inicial->aprender_movs($idi);
        $this->equipo_Pokemon[0] = $inicial;
        for ($index = 1; $index < 6; $index++) {
            //$consulta = new ConsultarApi();
            $rand = random_int(1, 802);
            //$json2 = $consulta->consultarPokemon();
            $json2 = $consulta->consultarPoke($rand);
            $id = $consulta->mostrarId($json2);
            $pokemon = new Pokemon($id);
            $name = $consulta->mostrarNombrePok($json2);
            $pokemon->setNombre($name);
            $pokemon->aprender_movs($id);
            
            $this->equipo_Pokemon[]= $pokemon;//$consulta->mostrarNombre($json);
            
            
        }
    }

    
}


//$var = new Entrenador("jhony", "m");
//$var->asignar_Equipo();

//echo $var->getEquipo_Pokemon()[0]->getMovimientos()[0];

//$var->getEquipo_Pokemon()[0]->getNombre();
//$var->getEquipo_Pokemon()[0]->getId();
//echo $var->getEquipo_Pokemon()[2];
//echo $var->getEquipo_Pokemon()[3];
//echo $var->getEquipo_Pokemon()[4];

