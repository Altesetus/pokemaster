<?php
include 'logica/conexion.php';
session_start();
error_reporting (0);
$op = $_POST["op"];
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    $now = time();
    if($now > $_SESSION['expire']) {
    session_destroy();
    }
}else { 
    header("Location: index.php");
}

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.png">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        
        <title>Pokémaster</title>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <section>           
            <div class="container">
                <div id="inicio">
                          <h2 class="sombraTexto">¡Hola <?php 
                          traerinfo();
                          echo $_SESSION["user"]
                          ?>!</h2><br>
                          <form action="perfil.php">
                            <input type="submit" value="Perfil de Entrenador">
                            
                          </form><br>
                          <form action="seleccionIA.php">
                            <input type="submit" value="Batalla VS. IA">                          
                          </form><br>
                          <form action="agregarIA.php">
                            <input type="submit" value="Agregar IA">                          
                          </form><br>
                          <form method="POST" action="index.php">
                              <input type="hidden" name="op" value="registro"/>
                            <input type="submit" value="Salir">
                          </form><br>
                </div>
              </div>           
        </section>
    </body>
</html>

<?php
    function traerinfo(){
        $conn = conectar();
        $usuario= $_SESSION["user"];
        $query="SELECT * FROM entrenador WHERE Nombre='$usuario'";
        $res= mysqli_query($conn, $query) or die (mysqli_error($conn)); //ejecuto el comando
        
        if ($res){ //.. si se ejecuto correctamente, el valor de $res no es falso
            if ($reg= mysqli_fetch_object($res)){ //obtengo todo el registro como un objeto
                $_SESSION["ID"]= $reg->ID;
                $_SESSION["Genero"]= $reg->Genero;
                //..  y almaceno el valor del objeto en la sesion
                desconectar();// cierro la conexion a la base de datos
                return true; //termino todo correctamente
    }
}
return false;// cierro la conexion a la base de datos
//si no devuelvo nada, la funcion retornara false.
}
    
?>
