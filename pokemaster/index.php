<?php
include 'logica/conexion.php';
error_reporting (0);
session_start(); //inicio las variables de sesion...
/* @var $_POST type */
$op = $_POST["op"]; //obtenemos el valor de la accion que se esta haciendo
if (isset($op) && $op == "login") { //si tiene valor y es 'login'...
    $ok = validar_ingreso();
} //.. validamos el ingreso
//sino.. mostrar el formulario
//$ok tendra TRUE si se logeo correctamente
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.png"> 
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        
        <title>Inicio de Sesión</title>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body id="body">
        <section>           
            <div class="container">
                <div id="logIn">
                          <h2 class="sombraTexto">Log In</h2>
                          <p>Accede a tu ficha de entrenador:</p>
                          <form method="post" action="<?php print($_SERVER["PHP_SELF"]);?>">
                              <input type="hidden" name="op" value="login" autocomplete="off"/>
                              <?php if($op && !$ok){ //si no se logeo correctamente, mostrar un mensaje de error
                                print("Usuario erróneo");
                                } ?>
                              <br>
                            Nombre Entrenador: <br><input type="text" name="usuario"> 
                            <br>
                            <input type="submit" value="Ingresar">                          
                          </form><br>
                          <form action="registro.php">
                          ¿No eres un entrenador aún?<br>
                            <input type="submit" value="Registrarse">
                          </form><br>
                </div>
              </div>           
        </section>
    </body>
</html>
<?php //se pueden poner scriplets en cualqueir parte del php
function validar_ingreso(){
$usuario=$_POST["usuario"]; //obtengo el parametro usuario del formulario...
$conn = conectar();
//creamos un comando SQL, notar que si pongo comillas dobles, el valor de las variables
//   son interpretadas como parte de la cadena
$query="SELECT * FROM entrenador WHERE Nombre='$usuario'";
$res=mysqli_query($conn, $query) or die (mysqli_error($conn)); //ejecuto el comando

if ($res ){ //.. si se ejecuto correctamente, el valor de $res no es falso

 if ($reg=mysqli_fetch_object($res)){ //obtengo todo el registro como un objeto
     $_SESSION["usuario"]= $reg; //..  y almaceno el valor del objeto en la sesion
     $_SESSION["user"]= $usuario;
      //y redirecciono al index de la aplicacion
     $_SESSION["loggedin"] = true;
     $_SESSION["start"] = time();
     $_SESSION["expire"] = $_SESSION['start'] + (5 * 60);
     header("Location: inicio.php");
     desconectar();// cierro la conexion a la base de datos
     return true; //termino todo correctamente
 }
}
return false;// cierro la conexion a la base de datos
//si no devuelvo nada, la funcion retornara false.
}
?>