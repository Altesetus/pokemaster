<?php
session_start();
include 'logica/conexion.php';
include 'logica/ConsultarApi.php';
error_reporting (0);

$op = $_POST["op"]; //obtenemos el valor de la accion que se esta haciendo
if (isset($op) && $op == "registro") {
    if(comprobar() == false){//si tiene valor y es 'login'...
    $ok = registrar();
    }else{
        header("Location: fallo2.php");
    }
}

function listado(){
    $consulta = new ConsultarApi();
    $json = $consulta->listaPokes();
    $pokes = json_decode($json, true);
    $consulta->llenarArrayPokes($json);
    
    for($index = 0; $index < 150; $index ++){
        $nombrepoke = "".$consulta->getPokemons()[$index];
    echo "<option value = '$index'>$nombrepoke</option>";
    }
    
}

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.png"> 
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        
        <title>Agregar IA</title>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <section>           
            <div class="container">
                <div id="equipo">
                          <h2 class="sombraTexto">Equipo Pokémon</h2>
                          <form method="POST" action="<?php print($_SERVER["PHP_SELF"]);?>"  autocomplete="off">
                              <input type="hidden" name="op" value="registro"/>
                          <p>Por favor, elija los 6 Pokémon del entrenador Artificial:
                          <table id="tabla1">
                              <tr id="th1">
                                  <td id="td2">
                                      <select>
                                      <?php 
                                      listado();
                                      ?>
                                      </select>
                                  </td><br>
                                  <td id="td2">
                                      <select>
                                        <option value="M">Cargando...</option>
                                      </select>
                                  </td><br>
                                  <td id="td2">
                                      <select>
                                        <option value="M">Cargando...</option>
                                      </select>
                                  </td><br>
                              </tr>
                              <tr id="th1">
                                  <td id="td2">
                                      <select>
                                        <option value="M">Cargando...</option>
                                      </select>
                                  </td><br>
                              <td id="td2">
                                      <select>
                                        <option value="M">Cargando...</option>
                                      </select>
                                  </td><br>
                                  <td id="td2">
                                      <select>
                                        <option value="M">Cargando...</option>
                                      </select>
                                  </td><br>
                              </tr>
                          </table>
                            <input type="submit" value="Guardar">
                          </form><br>
                          <form action="agregarIA.php">
                            <input type="submit" value="Cancelar">
                          </form><br>
                </div>
              </div>           
        </section>
    </body>
</html>
<?php
?>