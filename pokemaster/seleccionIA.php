<?php
set_time_limit(0);
session_start();
include 'logica/conexion.php';
include 'logica/ConsultarApi.php';
error_reporting (0);
if (isset($op) && $op == "registro") {
    session_destroy();

}if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    $now = time();
    if($now > $_SESSION['expire']) {
    session_destroy();
    }
}else { 
    header("Location: index.php");
}
/* @var $_POST type */
$op = $_POST["op"]; //obtenemos el valor de la accion que se esta haciendo
if (isset($op) && $op == "registro") {
    header("Location: resultados.php");
}

function listado(){
    $conn = conectar();
    
    $querysel = "SELECT * FROM entrenadorart ORDER BY Nivel DESC";
    $ressel = mysqli_query($conn, $querysel) or die (mysqli_error($conn)); //ejecuto el comando
        
    $cantidad = mysqli_num_rows($ressel);
    
    for($index = 0; $index < $cantidad; $index ++){
        
        if ($regsel= mysqli_fetch_object($ressel)){ //obtengo todo el registro como un objeto
        $_SESSION["NomDesafiado"] = $regsel->Nombre;
        $_SESSION["Identificacion"] = $regsel->ID;
        $_SESSION["Dificultad"] = $regsel->Nivel;
        }
        
        $nombreIA = $_SESSION["NomDesafiado"];
        $idIA = $_SESSION["Identificacion"];
        $dificultad = $_SESSION["Dificultad"];
        if($dificultad == 1){
            $dificult = "Fácil";
        }else if($dificultad == 2){
            $dificult = "Normal";
        }else if($dificultad == 3){
            $dificult = "Pokémaster";
        }
    echo "<option value = '$idIA'>$nombreIA - $dificult</option>";
    
    }
    
}

//.. validamos el ingreso
//sino.. mostrar el formulario
//$ok tendra TRUE si se logeo correctamente
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.png"> 
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        
        <title>Selección de </title>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <section>           
            <div class="container">
                <div id="registro" >
                          <h2 class="sombraTexto">Elige a tu contrincante</h2>
                          <form method="POST" action="<?php print($_SERVER["PHP_SELF"]);?>"  autocomplete="off">
                              <input type="hidden" name="op" value="registro"/>
                              <br>
                            <select name="Enemy">
                                <?php 
                                listado();
                                guardaenemigo();
                                ?>
                            </select><br><br>
                            <input type="submit" value="Duelo">                          
                          </form><br>
                          <form action="inicio.php">
                            <input type="submit" value="Cancelar">
                          </form><br>
                </div>
              </div>           
        </section>
    </body>
</html>
<?php
function guardaenemigo(){
    $contrincanteActual = $_POST["Enemy"];
    $_SESSION["DuelistaIA"] = $contrincanteActual;
}
?>