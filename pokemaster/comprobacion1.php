<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.png"> 
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        
        <title>¡Exito!</title>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body id="body">
        <section>           
            <div class="container">
                <div id="logIn">
                          <h2 class="sombraTexto">¡Vamos a ello!</h2>
                          <p>Entrenador Artificial creado</p>
                          <form action="inicio.php">
                            <input type="submit" value="¡No puedo esperar!">
                          </form><br>
                </div>
              </div>           
        </section>
    </body>
</html>