<?php
session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    $now = time();

if($now > $_SESSION['expire']) {
session_destroy();}
}else { 
    header("Location: index.php");
}

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.png">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        
        <title>Perfil de Entrenador</title>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <section>           
            <div class="container">
                <div id="perfil">
                          <h2 class="sombraTexto">Perfil de Entrenador</h2><br>
                          <p>ID de Entrenador: <?php echo $_SESSION["ID"] ?></p>
                          <p>Nombre de Entrenador: <?php echo $_SESSION["user"] ?></p>
                          <p>Género: <?php echo $_SESSION["Genero"] ?></p>
                          
                          <form action="historial.php">
                            <input type="submit" value="Historial de Batallas">                          
                          </form><br>
                          <form action="equipo.php">
                            <input type="submit" value="Equipo Pokemon">                          
                          </form><br>
                          <form action="inicio.php">
                            <input type="submit" value="Regresar">
                          </form><br>
                </div>
              </div>           
        </section>
    </body>
</html>
<?php
    
?>